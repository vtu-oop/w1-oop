﻿using System;
namespace w1_oop
{
	public class Laptop : Device
	{
        public Laptop(string brand, double screenSize, int batteryCapacity)
        : base(brand, screenSize, batteryCapacity, 0)
        {
            
        }

        public override void Charge()
        {
	        BatteryLevel += 10;
        }
	}
}

