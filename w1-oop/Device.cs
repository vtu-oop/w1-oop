﻿using System;
namespace w1_oop
{
	public abstract class Device
	{
		public string Brand { get; set; } 
		public double ScreenSize { get; set; }
		public int BatteryCapacity { get; set; }
		public int BatteryLevel { get; set; }

        protected Device(string brand, double screenSize, int batteryCapacity, int batteryLevel)
        {
			this.Brand = brand;
			this.ScreenSize = screenSize;
			this.BatteryCapacity = batteryCapacity;
			this.BatteryLevel = batteryLevel;
		}

		//abstract method
		//protected - accessible for the child/derived classes
		public abstract void Charge();

		//inherited from the Object class
		public override string ToString()
		{
			return $"{Brand} has {ScreenSize} screen with {BatteryCapacity} mAh battery charged to {BatteryLevel}%";
		}
	}
}

