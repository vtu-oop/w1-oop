﻿// See https://aka.ms/new-console-template for more information

using System.Threading.Channels;
using w1_oop;
using w1_oop.Models;

Laptop asus = new Laptop("Asus", 15.6, 20000);
Console.WriteLine(asus);
asus.Charge();
Console.WriteLine(asus);

Smartphone samsung = new Smartphone("Samsung", 5.2, 4000, 33);
samsung.Charge();
Console.WriteLine(samsung);

ExampleModel exampleModel = new ExampleModel();
Console.WriteLine(exampleModel); //printing the FQDN (fully-qualified domain name)

Device device = new Laptop("Macbook", 14.0, 50000);
Console.WriteLine(device);
device = samsung;
Console.WriteLine(device);