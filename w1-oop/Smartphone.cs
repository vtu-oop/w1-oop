﻿using System;

namespace w1_oop
{
	public class Smartphone : Device
	{
		public Smartphone (string brand, double screenSize, int batteryCapacity, int batteryLevel)
		: base(brand, screenSize, batteryCapacity, batteryLevel)
        {
			
        }
		
		public override void Charge()
		{
			BatteryLevel += 20;
			BatteryLevel = BatteryLevel > 100 ? 100 : BatteryLevel;
		}
	}
}

